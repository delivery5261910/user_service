CREATE TYPE "discount_type" AS ENUM ('sum', 'percent');
CREATE TYPE "work_hour" AS ENUM ('start', 'end');

CREATE TABLE IF NOT EXISTS "clients" (
    "id" uuid PRIMARY KEY,
    "first_name" varchar(30),
    "last_name" varchar(30),
    "phone" varchar(15),
    "date_of_birth" timestamp,
    "last_ordered_date" timestamp,
    "total_orders_sum" bigint DEFAULT 0,
    "total_orders_count" int DEFAULT 0,
    "discount_type" discount_type,
    "discount_amount" int,
    "searching_column" TEXT,
    "created_at" timestamp DEFAULT (now()),
    "updated_at" timestamp DEFAULT (now()),
    "deleted_at" int DEFAULT 0
);

CREATE TABLE IF NOT EXISTS "branches" (
    "id" uuid PRIMARY KEY,
    "name" VARCHAR (30),
    "phone" VARCHAR (13),
    "delivery_tariff" VARCHAR (40),
    "work_hour" work_hour,
    "address" VARCHAR (100),
    "destination" VARCHAR (50),
    "status" BOOL,
    "searching_column" TEXT,
    "created_at" timestamp DEFAULT (now()),
    "updated_at" TIMESTAMP,
    "deleted_at" int DEFAULT 0
);

CREATE TABLE IF NOT EXISTS "couriers" (
    "id" uuid PRIMARY KEY,
    "first_name" varchar(30),
    "last_name" varchar(30),
    "phone" varchar(15) UNIQUE,
    "status" bool DEFAULT true,
    "login" varchar(20),
    "password" varchar(60),
    "max_order_count" int DEFAULT 5,
    "branch_id" UUID REFERENCES branches(id),
    "verify_status" bool DEFAULT false,
    "searching_column" TEXT,
    "created_at" timestamp DEFAULT (now()),
    "updated_at" timestamp DEFAULT (now()),
    "deleted_at" int DEFAULT 0
);

CREATE TABLE IF NOT EXISTS verify_cods (
    id UUID PRIMARY KEY,
    phone VARCHAR(15) UNIQUE,
    code INT,
    created_at TIMESTAMP DEFAULT NOW()
);
