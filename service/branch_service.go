package service

import (
	"context"
	"fmt"
	"google.golang.org/protobuf/types/known/emptypb"
	pb "user_service/genproto/user_service"
	"user_service/grpc/client"
	"user_service/pkg/logger"
	"user_service/storage"
)

type branchService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
	pb.UnimplementedBranchServiceServer
}

func NewBranchService(strg storage.IStorage, services client.IServiceManager, log logger.ILogger) *branchService {
	return &branchService{
		storage:  strg,
		services: services,
		log:      log,
	}
}

func (h *branchService) Create(ctx context.Context, request *pb.CreateBranchRequest) (*pb.Branch, error) {
	branch, err := h.storage.Branch().Create(ctx, request)
	if err != nil {
		fmt.Println("Err 1", err.Error())
		return nil, err
	}

	return branch, nil
}

func (h *branchService) Get(ctx context.Context, key *pb.PrimaryKey) (*pb.Branch, error) {
	fmt.Println(key)
	return h.storage.Branch().Get(ctx, key)
}

func (h *branchService) GetList(ctx context.Context, request *pb.GetListRequest) (*pb.BranchResponse, error) {
	return h.storage.Branch().GetList(ctx, request)
}

func (h *branchService) Update(ctx context.Context, branch *pb.Branch) (*pb.Branch, error) {
	return h.storage.Branch().Update(ctx, branch)
}

func (h *branchService) Delete(ctx context.Context, key *pb.PrimaryKey) (*emptypb.Empty, error) {
	return h.storage.Branch().Delete(ctx, key)
}
