package service

import (
	"context"
	"fmt"
	"google.golang.org/protobuf/types/known/emptypb"
	pb "user_service/genproto/user_service"
	"user_service/grpc/client"
	"user_service/pkg/logger"
	"user_service/storage"
)

type clientService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
	pb.UnimplementedClientServiceServer
}

func NewClientService(strg storage.IStorage, services client.IServiceManager, log logger.ILogger) *clientService {
	return &clientService{
		storage:  strg,
		services: services,
		log:      log,
	}
}

func (c *clientService) Create(ctx context.Context, request *pb.CreateClientRequest) (*pb.Client, error) {
	user, err := c.storage.Client().Create(ctx, request)
	if err != nil {
		fmt.Println("Err 1", err.Error())
		return nil, err
	}

	return user, nil
}

func (c *clientService) Get(ctx context.Context, key *pb.PrimaryKey) (*pb.Client, error) {
	fmt.Println(key)
	return c.storage.Client().Get(ctx, key)
}

func (c *clientService) GetList(ctx context.Context, request *pb.GetListRequest) (*pb.ClientResponse, error) {
	return c.storage.Client().GetList(ctx, request)
}

func (c *clientService) Update(ctx context.Context, client *pb.Client) (*pb.Client, error) {
	return c.storage.Client().Update(ctx, client)
}

func (c *clientService) Delete(ctx context.Context, key *pb.PrimaryKey) (*emptypb.Empty, error) {
	return c.storage.Client().Delete(ctx, key)
}
