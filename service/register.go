package service

import (
	"context"
	pb "user_service/genproto/user_service"
	"user_service/grpc/client"
	"user_service/pkg/logger"
	"user_service/storage"
)

type registerService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
	pb.UnimplementedRegisterServiceServer
}

func NewRegisterService(storage storage.IStorage, services client.IServiceManager, log logger.ILogger) *registerService {
	return &registerService{
		storage: storage,
		services: services,
		log:     log,
	}
}

func (r registerService) Create(ctx context.Context, verify *pb.CreateRegisterRequest) (*pb.Register, error) {
	r.log.Info("category create service layer", logger.Any("verify", verify))


	code, err := r.storage.Register().Get(ctx, verify.Phone)
	if err != nil {
		r.log.Error("ERROR in service layer while get code by phone", logger.Error(err))
		return nil, err
	}

	if code.Code == verify.Code {
		err := r.storage.Register().UpdateStatus(ctx, verify.Phone)
		if err != nil {
			r.log.Error("ERROR in service layer while updating verify status by phone", logger.Error(err))
			return nil, err
		}
	}

	return nil, nil
}