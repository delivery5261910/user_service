package service

import (
	"context"
	"fmt"
	pb "user_service/genproto/user_service"
	"user_service/grpc/client"
	"user_service/pkg/logger"
	"user_service/pkg/security"
	"user_service/pkg/sms"
	"user_service/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type courierService struct {
	storage  storage.IStorage
	services client.IServiceManager
	log      logger.ILogger
	pb.UnimplementedCourierServiceServer
}

func NewCourierService(strg storage.IStorage, services client.IServiceManager, log logger.ILogger) *courierService {
	return &courierService{
		storage:  strg,
		services: services,
		log:      log,
	}
}

func (c *courierService) Create(ctx context.Context, request *pb.CreateCourierRequest) (*pb.Courier, error) {

	code := sms.GenerateVerificationCode()

	if err := sms.Send(request.Phone, code); err != nil {
		c.log.Error("Error while sending verification code", logger.Error(err))
		return nil, err
	}

	err := c.storage.Register().Create(ctx, pb.CreateRegisterRequest{
		Phone: request.Phone,
		Code: code,
	})
	if err != nil {
		c.log.Error("ERROR in service layer while creating category", logger.Error(err))
		return nil, err
	}

	password, err := security.HashPassword(request.Password)
	if err != nil {
		c.log.Error("Error while hashing password", logger.Error(err))
		return nil, err
	}

	request.Password = password

	user, err := c.storage.Courier().Create(ctx, request)
	if err != nil {
		fmt.Println("Err 1", err.Error())
		return nil, err
	}

	return user, nil
}

func (c *courierService) Get(ctx context.Context, key *pb.PrimaryKey) (*pb.Courier, error) {
	fmt.Println(key)
	return c.storage.Courier().Get(ctx, key)
}

func (c *courierService) GetList(ctx context.Context, request *pb.GetListRequest) (*pb.CourierResponse, error) {
	return c.storage.Courier().GetList(ctx, request)
}

func (c *courierService) Update(ctx context.Context, courier *pb.Courier) (*pb.Courier, error) {
	return c.storage.Courier().Update(ctx, courier)
}

func (c *courierService) Delete(ctx context.Context, key *pb.PrimaryKey) (*emptypb.Empty, error) {
	return c.storage.Courier().Delete(ctx, key)
}

func (c *courierService) UpdatePasswordRequest(ctx context.Context, request *pb.UpdatePassword) (*emptypb.Empty, error) {
	oldPassword, err := c.storage.Courier().GetPassword(ctx, request)
	if err != nil {
		c.log.Error("ERROR in service layer while getting courier password", logger.Error(err))
		return nil, err
	}

	if oldPassword != request.OldPassword {
		c.log.Error("ERROR in service old password is not correct")
		return nil, err
	}

	if err = c.storage.Courier().UpdatePasswordRequest(context.Background(), request); err != nil {
		c.log.Error("ERROR in service layer while updating password", logger.Error(err))
		return nil, err
	}

	return nil, nil
}