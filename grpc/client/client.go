package client

import (
	"google.golang.org/grpc"
	"user_service/config"
	"user_service/genproto/user_service"
)

type IServiceManager interface {
	BranchService() user_service.BranchServiceClient
	ClientService() user_service.ClientServiceClient
	CourierService() user_service.CourierServiceClient
	RegisterService() user_service.RegisterServiceClient
}

type grpcClients struct {
	categoryService user_service.BranchServiceClient
	clientService  user_service.ClientServiceClient
	courierService user_service.CourierServiceClient
	registerService user_service.RegisterServiceClient
}

func NewGrpcClients(cfg config.Config) (IServiceManager, error) {
	connUserService, err := grpc.Dial(
		cfg.ServiceGrpcHost+cfg.ServiceGrpcPort,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		categoryService: user_service.NewBranchServiceClient(connUserService),
		clientService:  user_service.NewClientServiceClient(connUserService),
		courierService: user_service.NewCourierServiceClient(connUserService),
		registerService: user_service.NewRegisterServiceClient(connUserService),
	}, nil
}

func (g *grpcClients) BranchService() user_service.BranchServiceClient {
	return g.categoryService
}

func (g *grpcClients) ClientService() user_service.ClientServiceClient {
	return g.clientService
}

func (g *grpcClients) CourierService() user_service.CourierServiceClient {
	return g.courierService
}

func (g *grpcClients) RegisterService() user_service.RegisterServiceClient {
	return g.registerService
}
