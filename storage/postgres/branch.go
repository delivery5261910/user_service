package postgres

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"
	pb "user_service/genproto/user_service"
	"user_service/pkg/helper"
	"user_service/pkg/logger"
	"user_service/storage"
)

type branchRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewBranchRepo(db *pgxpool.Pool, log logger.ILogger) storage.IBranchStorage {
	return &branchRepo{
		db:  db,
		log: log,
	}
}

func (b *branchRepo) Create(ctx context.Context, request *pb.CreateBranchRequest) (*pb.Branch, error) {
	var (
		branch          = pb.Branch{}
		err             error
		searchingColumn string
	)

	query := `
	insert into branches (id, name, phone, delivery_tariff, work_hour, address, destination, status, searching_column)
    values ($1, $2, $3, $4, $5, $6, $7, $8, $9)
    returning id, name, phone, delivery_tariff, work_hour, address, destination, status
`

	searchingColumn = fmt.Sprintf("%s %s %s %s %s %s %v", request.Name, request.Phone, request.DeliveryTariff, request.WorkHour, request.Address, request.Destination, request.Status)

	if err = b.db.QueryRow(ctx, query,
		uuid.New().String(),
		request.Name,
		request.Phone,
		request.DeliveryTariff,
		request.WorkHour,
		request.Address,
		request.Destination,
		request.Status,
		searchingColumn).
		Scan(
			&branch.Id,
			&branch.Name,
			&branch.Phone,
			&branch.DeliveryTariff,
			&branch.WorkHour,
			&branch.Address,
			&branch.Destination,
			&branch.Status,
		); err != nil {
		fmt.Println("err 3", err.Error())
		return nil, err
	}

	return &branch, nil
}

func (b *branchRepo) Get(ctx context.Context, key *pb.PrimaryKey) (*pb.Branch, error) {
	var (
		branch = pb.Branch{}
	)

	query := `select id, name, phone, delivery_tariff, work_hour, address, destination, status from branches where deleted_at = 0 and id = $1`

	if err := b.db.QueryRow(ctx, query, key.GetId()).Scan(
		&branch.Id,
		&branch.Name,
		&branch.Phone,
		&branch.DeliveryTariff,
		&branch.WorkHour,
		&branch.Address,
		&branch.Destination,
		&branch.Status,
	); err != nil {
		b.log.Error("error while scanning branch by id", logger.Error(err))
		return nil, err
	}

	return &branch, nil
}

func (b *branchRepo) GetList(ctx context.Context, request *pb.GetListRequest) (*pb.BranchResponse, error) {
	var (
		resp   = pb.BranchResponse{}
		filter = "where deleted_at = 0 "
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
	)

	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%s' ", request.GetSearch())
	}

	countQuery := `select count(1) from branches ` + filter

	if err := b.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		b.log.Error("error while scanning count of branches", logger.Error(err))
		return nil, err
	}

	query := `select id, name, phone, delivery_tariff, work_hour, address, destination, status from branches ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())

	rows, err := b.db.Query(ctx, query)
	if err != nil {
		b.log.Error("error while getting branch rows", logger.Error(err))
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			branch = pb.Branch{}
		)

		if err = rows.Scan(
			&branch.Id,
			&branch.Name,
			&branch.Phone,
			&branch.DeliveryTariff,
			&branch.WorkHour,
			&branch.Address,
			&branch.Destination,
			&branch.Status,
		); err != nil {
			b.log.Error("error while scanning ony by one branch", logger.Error(err))
			return nil, err
		}

		resp.Branches = append(resp.Branches, &branch)
	}

	resp.Count = count

	return &resp, nil
}

func (b *branchRepo) Update(ctx context.Context, branch *pb.Branch) (*pb.Branch, error) {
	var (
		resp   = pb.Branch{}
		params = make(map[string]interface{})
		query  = `update branches set `
		filter = ""
	)

	params["id"] = branch.GetId()

	if branch.GetName() != "" {

		params["name"] = branch.GetName()

		filter += " name = @name,"
	}

	if branch.GetPhone() != "" {

		params["phone"] = branch.GetPhone()

		filter += " phone = @phone,"
	}

	if branch.GetDeliveryTariff() != "" {

		params["delivery_tariff"] = branch.GetDeliveryTariff()

		filter += " delivery_tariff = @delivery_tariff,"
	}

	if branch.GetWorkHour() != "" {

		params["work_hour"] = branch.GetWorkHour()

		filter += " work_hour = @work_hour,"
	}

	if branch.GetAddress() != "" {

		params["address"] = branch.GetAddress()

		filter += " address = @address,"
	}

	if branch.GetDestination() != "" {

		params["destination"] = branch.GetDestination()

		filter += " destination = @destination,"
	}

	if branch.GetStatus() {
		params["status"] = branch.GetStatus()

		filter += " status = @status,"
	}

	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id`

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := b.db.QueryRow(ctx, fullQuery, args...).Scan(
		&resp.Id,
	); err != nil {
		return nil, err
	}

	return &resp, nil
}

func (b *branchRepo) Delete(ctx context.Context, key *pb.PrimaryKey) (*emptypb.Empty, error) {
	_, err := b.db.Exec(ctx, `update branches set deleted_at = extract(epoch from current_timestamp) where id = $1`, key.GetId())

	return nil, err
}
