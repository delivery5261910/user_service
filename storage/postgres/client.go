package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"
	"time"
	pb "user_service/genproto/user_service"
	"user_service/pkg/helper"
	"user_service/pkg/logger"
	"user_service/storage"
)

type clientRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewClientRepo(db *pgxpool.Pool, log logger.ILogger) storage.IClientStorage {
	return &clientRepo{
		db:  db,
		log: log,
	}
}

func (c *clientRepo) Create(ctx context.Context, request *pb.CreateClientRequest) (*pb.Client, error) {
	var (
		client          = pb.Client{}
		err             error
		searchingColumn string
	)

	dateOfBirth, err := time.Parse("2006-01-02", request.DateOfBirth)
	if err != nil {
		return nil, err
	}

	query := `
		insert into clients (id, first_name, last_name, phone, date_of_birth, searching_column)
			values ($1, $2, $3, $4, $5, $6)
			returning id, first_name, last_name, phone, date_of_birth`

	searchingColumn = fmt.Sprintf("%s %s %s %s", request.FirstName, request.LastName, request.Phone, dateOfBirth)

	if err = c.db.QueryRow(ctx, query,
		uuid.New().String(),
		request.FirstName,
		request.LastName,
		request.Phone,
		dateOfBirth,
		searchingColumn).
		Scan(
			&client.Id,
			&client.FirstName,
			&client.LastName,
			&client.Phone,
			&dateOfBirth,
		); err != nil {
		fmt.Println("err 3", err.Error())
		return nil, err
	}

	return &client, nil
}

func (c *clientRepo) Get(ctx context.Context, key *pb.PrimaryKey) (*pb.Client, error) {
	var (
		client                                                     = pb.Client{}
		dateOfBirth, lastOrderedDate, discountType, discountAmount sql.NullString
	)

	query := `select id, first_name, last_name, phone, date_of_birth, last_ordered_date, total_orders_sum, total_orders_count, discount_type, discount_amount from clients where deleted_at = 0 and id = $1`

	if err := c.db.QueryRow(ctx, query, key.GetId()).Scan(
		&client.Id,
		&client.FirstName,
		&client.LastName,
		&client.Phone,
		&dateOfBirth,
		&lastOrderedDate,
		&client.TotalOrdersSum,
		&client.TotalOrdersCount,
		&discountType,
		&discountAmount,
	); err != nil {
		c.log.Error("error while scanning client by id", logger.Error(err))
		return nil, err
	}

	if dateOfBirth.Valid {
		client.DateOfBirth = dateOfBirth.String
	}

	if lastOrderedDate.Valid {
		client.LastOrderedDate = lastOrderedDate.String
	}

	if discountType.Valid {
		client.DiscountType = discountType.String
	}

	if discountAmount.Valid {
		client.DiscountAmount = discountAmount.String
	}

	return &client, nil
}

func (c *clientRepo) GetList(ctx context.Context, request *pb.GetListRequest) (*pb.ClientResponse, error) {
	var (
		resp                                                       = pb.ClientResponse{}
		filter                                                     = "where deleted_at = 0 "
		offset                                                     = (request.GetPage() - 1) * request.GetLimit()
		count                                                      = int32(0)
		dateOfBirth, lastOrderedDate, discountType, discountAmount sql.NullString
	)

	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%s' ", request.GetSearch())
	}

	countQuery := `select count(1) from clients ` + filter

	if err := c.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		c.log.Error("error while scanning count of clients", logger.Error(err))
		return nil, err
	}

	query := `select id, first_name, last_name, phone, date_of_birth, last_ordered_date, total_orders_sum, total_orders_count, discount_type, discount_amount from clients ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())

	rows, err := c.db.Query(ctx, query)
	if err != nil {
		c.log.Error("error while getting client rows", logger.Error(err))
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			client = pb.Client{}
		)

		if err = rows.Scan(
			&client.Id,
			&client.FirstName,
			&client.LastName,
			&client.Phone,
			&dateOfBirth,
			&lastOrderedDate,
			&client.TotalOrdersSum,
			&client.TotalOrdersCount,
			&discountType,
			&discountAmount,
		); err != nil {
			c.log.Error("error while scanning ony by one client", logger.Error(err))
			return nil, err
		}

		if dateOfBirth.Valid {
			client.DateOfBirth = dateOfBirth.String
		}

		if lastOrderedDate.Valid {
			client.LastOrderedDate = lastOrderedDate.String
		}

		if discountType.Valid {
			client.DiscountType = discountType.String
		}

		if discountAmount.Valid {
			client.DiscountAmount = discountAmount.String
		}

		resp.Clients = append(resp.Clients, &client)
	}

	resp.Count = count

	return &resp, nil
}

func (c *clientRepo) Update(ctx context.Context, client *pb.Client) (*pb.Client, error) {
	var (
		resp   = pb.Client{}
		params = make(map[string]interface{})
		query  = `update clients set `
		filter = ""
	)

	params["id"] = client.GetId()

	if client.GetPhone() != "" {

		params["phone"] = client.GetPhone()

		filter += " phone = @phone,"
	}

	if client.GetLastOrderedDate() != "" {
		params["last_ordered_date"] = client.GetLastOrderedDate()

		filter += " last_ordered_date = @last_ordered_date,"
	}

	if client.GetTotalOrdersSum() != "" {
		params["total_orders_sum"] = client.GetTotalOrdersSum()

		filter += " total_orders_sum = @total_orders_sum,"
	}

	if client.GetTotalOrdersCount() != 0 {
		params["total_orders_count"] = client.GetTotalOrdersCount()

		filter += " total_orders_count = @total_orders_count,"
	}

	if client.GetDiscountType() != "" {
		params["discount_type"] = client.GetDiscountType()

		filter += " discount_type = @discount_type,"
	}

	if client.GetDiscountAmount() != "" {
		params["discount_amount"] = client.GetDiscountAmount()

		filter += " discount_amount = @discount_amount,"
	}

	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id`

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := c.db.QueryRow(ctx, fullQuery, args...).Scan(
		&resp.Id,
	); err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *clientRepo) Delete(ctx context.Context, key *pb.PrimaryKey) (*emptypb.Empty, error) {
	_, err := c.db.Exec(ctx, `update clients set deleted_at = extract(epoch from current_timestamp) where id = $1`, key.GetId())

	return nil, err
}
