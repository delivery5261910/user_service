package postgres

import (
	"context"
	"fmt"
	pb "user_service/genproto/user_service"
	"user_service/pkg/helper"
	"user_service/pkg/logger"
	"user_service/storage"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/protobuf/types/known/emptypb"
)

type courierRepo struct {
	db  *pgxpool.Pool
	log logger.ILogger
}

func NewCourierRepo(db *pgxpool.Pool, log logger.ILogger) storage.ICourierStorage {
	return &courierRepo{
		db:  db,
		log: log,
	}
}

func (c *courierRepo) Create(ctx context.Context, request *pb.CreateCourierRequest) (*pb.Courier, error) {
	var (
		courier         = pb.Courier{}
		err             error
		searchingColumn string
	)

	query := `
		insert into couriers (id, first_name, last_name, phone, login, password, branch_id, searching_column)
			values ($1, $2, $3, $4, $5, $6, $7, $8)
			returning id, first_name, last_name, phone, status, login, branch_id`

	searchingColumn = fmt.Sprintf("%s %s %s %s", request.FirstName, request.LastName, request.Phone, request.Login)

	if err = c.db.QueryRow(ctx, query,
		uuid.New().String(),
		request.FirstName,
		request.LastName,
		request.Phone,
		request.Login,
		request.Password,
		request.BranchId,
		searchingColumn).
		Scan(
			&courier.Id,
			&courier.FirstName,
			&courier.LastName,
			&courier.Phone,
			&courier.Status,
			&courier.Login,
			&courier.BranchId,
		); err != nil {
		fmt.Println("err 3", err.Error())
		return nil, err
	}

	return &courier, nil
}

func (c *courierRepo) Get(ctx context.Context, key *pb.PrimaryKey) (*pb.Courier, error) {
	var (
		courier = pb.Courier{}
	)

	query := `select id, first_name, last_name, phone, status, login, max_order_count from couriers where deleted_at = 0 and id = $1`

	if err := c.db.QueryRow(ctx, query, key.GetId()).Scan(
		&courier.Id,
		&courier.FirstName,
		&courier.LastName,
		&courier.Phone,
		&courier.Status,
		&courier.Login,
		&courier.MaxOrderCount,
		&courier.BranchId,
	); err != nil {
		c.log.Error("error while scanning courier by id", logger.Error(err))
		return nil, err
	}

	return &courier, nil
}

func (c *courierRepo) GetList(ctx context.Context, request *pb.GetListRequest) (*pb.CourierResponse, error) {
	var (
		resp   = pb.CourierResponse{}
		filter = "where deleted_at = 0 "
		offset = (request.GetPage() - 1) * request.GetLimit()
		count  = int32(0)
	)

	if request.Search != "" {
		filter += fmt.Sprintf(" and searching_column ilike '%s' ", request.GetSearch())
	}

	countQuery := `select count(1) from couriers ` + filter

	if err := c.db.QueryRow(ctx, countQuery).Scan(&count); err != nil {
		c.log.Error("error while scanning count of couriers", logger.Error(err))
		return nil, err
	}

	query := `select id, first_name, last_name, phone, status, login, max_order_count from couriers ` + filter + fmt.Sprintf("offset %d limit %d", offset, request.GetLimit())

	rows, err := c.db.Query(ctx, query)
	if err != nil {
		c.log.Error("error while getting courier rows", logger.Error(err))
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			courier = pb.Courier{}
		)

		if err = rows.Scan(
			&courier.Id,
			&courier.FirstName,
			&courier.LastName,
			&courier.Phone,
			&courier.Status,
			&courier.Login,
			&courier.MaxOrderCount,
			&courier.BranchId,
		); err != nil {
			c.log.Error("error while scanning ony by one courier", logger.Error(err))
			return nil, err
		}

		resp.Couriers = append(resp.Couriers, &courier)
	}

	resp.Count = count

	return &resp, nil
}

func (c *courierRepo) Update(ctx context.Context, courier *pb.Courier) (*pb.Courier, error) {
	var (
		resp   = pb.Courier{}
		params = make(map[string]interface{})
		query  = `update couriers set `
		filter = ""
	)

	params["id"] = courier.GetId()

	if courier.GetPhone() != "" {

		params["phone"] = courier.GetPhone()

		filter += " phone = @phone,"
	}

	if courier.GetLogin() != "" {
		params["login"] = courier.GetLogin()

		filter += " login = @login,"
	}

	if courier.GetBranchId() != "" {
		params["branch_id"] = courier.GetBranchId()

		filter += " branch_id = @branch_id,"
	}

	if courier.GetMaxOrderCount() != 0 {
		params["max_order_count"] = courier.GetMaxOrderCount()

		filter += " max_order_count = @max_order_count,"
	}

	query += filter + ` updated_at = now() where deleted_at = 0 and id = @id returning id`

	fullQuery, args := helper.ReplaceQueryParams(query, params)

	if err := c.db.QueryRow(ctx, fullQuery, args...).Scan(
		&resp.Id,
	); err != nil {
		return nil, err
	}

	return &resp, nil
}

func (c *courierRepo) Delete(ctx context.Context, key *pb.PrimaryKey) (*emptypb.Empty, error) {
	_, err := c.db.Exec(ctx, `update couriers set deleted_at = extract(epoch from current_timestamp) where id = $1`, key.GetId())

	return nil, err
}

func (c *courierRepo) GetPassword(ctx context.Context, request *pb.UpdatePassword) (string, error) {
	password := ""

	query := `
		select password from couriers 
		                where login = $1`

	if err := c.db.QueryRow(ctx, query, request.Login).Scan(&password); err != nil {
		fmt.Println("Error while scanning password from couriers", err.Error())
		return "", err
	}

	return password, nil
}

func (c *courierRepo) UpdatePasswordRequest(ctx context.Context, request *pb.UpdatePassword) error {
	query := `
		update users 
				set password = $1, updated_at = now()
					where login = $2`

	if _, err := c.db.Exec(ctx, query, request.NewPassword, request.Login); err != nil {
		fmt.Println("error while updating password for user", err.Error())
		return err
	}

	return nil
}
