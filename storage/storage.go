package storage

import (
	"context"
	"google.golang.org/protobuf/types/known/emptypb"
	pb "user_service/genproto/user_service"
)

type IStorage interface {
	Close()
	Client() IClientStorage
	Branch() IBranchStorage
	Courier() ICourierStorage
	Register() IRegisterStorage
}

type IClientStorage interface {
	Create(context.Context, *pb.CreateClientRequest) (*pb.Client, error)
	Get(context.Context, *pb.PrimaryKey) (*pb.Client, error)
	GetList(context.Context, *pb.GetListRequest) (*pb.ClientResponse, error)
	Update(context.Context, *pb.Client) (*pb.Client, error)
	Delete(context.Context, *pb.PrimaryKey) (*emptypb.Empty, error)
}

type IBranchStorage interface {
	Create(context.Context, *pb.CreateBranchRequest) (*pb.Branch, error)
	Get(context.Context, *pb.PrimaryKey) (*pb.Branch, error)
	GetList(context.Context, *pb.GetListRequest) (*pb.BranchResponse, error)
	Update(context.Context, *pb.Branch) (*pb.Branch, error)
	Delete(context.Context, *pb.PrimaryKey) (*emptypb.Empty, error)
}

type ICourierStorage interface {
	Create(context.Context, *pb.CreateCourierRequest) (*pb.Courier, error)
	Get(context.Context, *pb.PrimaryKey) (*pb.Courier, error)
	GetList(context.Context, *pb.GetListRequest) (*pb.CourierResponse, error)
	Update(context.Context, *pb.Courier) (*pb.Courier, error)
	Delete(context.Context, *pb.PrimaryKey) (*emptypb.Empty, error)
	GetPassword(context.Context, *pb.UpdatePassword) (string, error)
	UpdatePasswordRequest(context.Context, *pb.UpdatePassword) error
}

type IRegisterStorage interface {
	Create(context.Context, pb.CreateRegisterRequest) error
	Get(context.Context, string) (pb.Register, error)
	UpdateStatus(context.Context, string) error
}
